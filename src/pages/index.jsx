export function Formulario() {
  return (
    <div>
      <label for="name">
        Digite la cantidad de integrantes para cada grupo:
      </label>
      <input
        type="text"
        id="name"
        name="name"
        required
        minlength="4"
        maxlength="8"
        size="10"
      ></input>
    </div>
  );
}
